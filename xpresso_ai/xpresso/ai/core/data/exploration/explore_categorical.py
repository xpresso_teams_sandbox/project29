__all__ = ["ExploreCategory"]
__author__ = ["Srijan Sharma"]

import numpy as np
from xpresso.ai.core.commons.utils.constants import DEFAULT_OUTLIER_THRESHOLD_CATEGORICAL


class ExploreCategory():

    def __init__(self, data, outlier_threshold=DEFAULT_OUTLIER_THRESHOLD_CATEGORICAL):
        self.data = data
        self.outlier_threshold = outlier_threshold
        return

    def populate_category(self):
        """Sets the metrics for categorical attribute
        Args:
        Return:
            """
        resp = dict()
        outliers, freq_count, mode = self.categorical_analysis(self.data)
        resp["outliers"] = outliers
        resp["freq_count"] = freq_count
        resp["mode"] = mode
        return resp

    @staticmethod
    def categorical_analysis(data, outlier_threshold=DEFAULT_OUTLIER_THRESHOLD_CATEGORICAL):
        """
        This function calculates outliers and frequency count for categorical data
        Args:
            outlier_threshold (:int): Number of outliers allowed
        Returns:
            """
        num_rows = float(data.size)
        freq_count = data.value_counts().to_dict()
        outliers = list()
        for label in freq_count.keys():
            if (freq_count[label] / num_rows) * 100 < outlier_threshold:
                outliers.append(label)
        mode = data.mode()
        if len(mode) >= 1:
            mode = mode[0]
        else:
            mode = "na"
            print("Unable to find mode for {}.".format(data.name))
        return np.array(outliers), freq_count, mode
