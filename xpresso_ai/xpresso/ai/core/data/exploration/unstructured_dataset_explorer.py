__all__ = ['UnstructuredDatasetExplorer']
__author__ = 'Srijan Sharma'

import os

import pandas as pd

from xpresso.ai.core.commons.exceptions.xpr_exceptions import \
    InvalidDatatypeException
from xpresso.ai.core.commons.utils.constants import \
    EXPLORE_MULTIVARIATE_FILENAME, \
    DEFAULT_PROBABILITY_BINS
from xpresso.ai.core.commons.utils.constants import \
    EXPLORE_UNIVARIATE_FILENAME, \
    EXPLORER_OUTPUT_PATH
from xpresso.ai.core.commons.utils.constants import FILE_PATH_COL, FILE_SIZE_COl
from xpresso.ai.core.data.automl.attribute_info import AttributeInfo
from xpresso.ai.core.data.automl.data_type import DataType
from xpresso.ai.core.data.automl.dataset_type import DatasetType
from xpresso.ai.core.data.exploration.explore_files import ExploreFiles
from xpresso.ai.core.data.exploration.explore_text import ExploreText
from xpresso.ai.core.data.exploration.render_exploration import \
    RenderExploration
from xpresso.ai.core.logging.xpr_log import XprLogger

# This is indented as logger can not be serialized and can not be part
# of automl
logger = XprLogger()


class UnstructuredDatasetExplorer:

    def __init__(self, dataset):
        self.dataset = dataset

    def understand(self, verbose=True):
        """
        Understands and assigns the datatype to the attributes for structured dataset
        Args:
            verbose('bool'): If True then renders the output
        """
        if self.dataset.type != DatasetType.UTEXT:
            logger.error("Unacceptable Data type provided. Type {} is "
                         "not supported".format(self.dataset.type))
            raise InvalidDatatypeException("Provided Data Type : {} not "
                                           "supported".format(
                self.dataset.type))
        self.understand_attributes()

        RenderExploration(self.dataset).render_understand(verbose=verbose)

    def explore_univariate(self, verbose=None, to_excel=None,
                           validity_threshold=None,
                           output_path=None,
                           file_name=None,
                           bins=None):
        """Unsupported"""
        raise InvalidDatatypeException("Provided Data Type : {} not "
                                       "supported".format(self.dataset.type))

    def explore_multivariate(self, verbose=None, to_excel=None,
                             output_path=None,
                             file_name=None):
        """Unsupported"""
        raise InvalidDatatypeException("Provided Data Type : {} not "
                                       "supported".format(self.dataset.type))

    def explore_unstructured(self, verbose=True, to_excel=False,
                             output_path=None, file_name=None):
        """
        Multivariate analysis of all the attributes
        Args:
            verbose('bool'): If True then renders the output
            to_excel('bool'): If True saves the output to excel file
            output_path(str): path where the multivariate excel file to be
                stored
            file_name(str): file name of excel file
        """

        if (self.dataset.type != DatasetType.UIMAGE and self.dataset.type !=
                DatasetType.UTEXT):
            logger.warning(
                "Unsupported datatype provided to explore_unstructured")
            raise InvalidDatatypeException("Provided Data Type : {} not "
                                           "supported".format(
                self.dataset.type))

        self.populate_unstructured()
        RenderExploration(self.dataset).render_unstructured(verbose=verbose,
                                                            to_excel=to_excel,
                                                            output_path=output_path,
                                                            file_name=file_name)

    def understand_attributes(self):
        """Populates the filesize distribution"""
        attr_name = "Size"
        size_col_name = FILE_SIZE_COl
        self.dataset.info.attributeInfo = AttributeInfo(attr_name)
        if size_col_name not in self.dataset.data.columns:
            logger.warning("File size information not found in the provided "
                           "DataFrame")
            return
        self.dataset.info.attributeInfo.metrics = ExploreFiles(
            self.dataset.data[size_col_name]).populate_filesize()

    def populate_unstructured(self):
        """Populates the metric for the Unstructured Dataset."""
        if not len(self.dataset.data):
            logger.warning("Unable to perform Unstructured  text data "
                           "analytics. No files present in the dataframe")
        if self.dataset.type == DatasetType.UTEXT:
            file_path = FILE_PATH_COL
            for index, row in self.dataset.data.iterrows():
                text_file_abspath = os.path.abspath(os.path.join(
                    os.getcwd(), row[file_path]))
                try:
                    text_data = open(text_file_abspath, "r").read()
                    self.populate_ngram(text_data)
                except Exception:
                    logger.error(
                        "Unable to read file {}".format(text_file_abspath))
        elif type is DatasetType.UIMAGE:
            print("Unstructured Image type not supported")
            logger.warning("Unstructured Image type not supported")

    def populate_ngram(self, text_data):
        """Performs n-gram analysis for text
        Args:
            text_data(str): Text data to be analysed """
        ngram_analysis = ExploreText(text_data).populate_text()
        for ngram in ngram_analysis.keys():
            if ngram not in self.dataset.info.metrics.keys():
                self.dataset.info.metrics[ngram] = dict()
            for ngram_value, ngram_count in ngram_analysis[ngram].items():
                if ngram_value not in self.dataset.info.metrics[ngram].keys():
                    self.dataset.info.metrics[ngram][ngram_value] = ngram_count
                else:
                    self.dataset.info.metrics[ngram][ngram_value] = \
                        self.dataset.info.metrics[ngram][
                            ngram_value] + ngram_count
